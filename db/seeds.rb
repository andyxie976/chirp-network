# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user_alice = User.create({ slug: "alice", name: "Alice", email: "alice@muensterschild.de", password: "123123", password_confirmation: "123123" })
user_alice.image.attach(io: File.open("app/assets/images/alice.png"), filename: "alice.png")
user_versicherungsmanager = User.create({ slug: "versicherungsmanager", csrf_token: "52750b30ffbc7de3b361", name: "Versicherungsmanager", email: "versicherungsmanager@muensterschild.de", password: "asdasd", password_confirmation: "asdasd" })
user_versicherungsmanager.image.attach(io: File.open("app/assets/images/duck.png"), filename: "duck.png")
user_blackhat = User.create({ slug: "blackhat", name: "Blackhat", email: "blackhat@malware.de", password: "qweqwe", password_confirmation: "qweqwe" })
user_blackhat.image.attach(io: File.open("app/assets/images/purge.jpg"), filename: "purge.jpg")
user_eve = User.create({ slug: "eve",name: "Eve", email: "Eve@evil.de", password: "yxcyxc", password_confirmation: "yxcyxc" })
user_eve.image.attach(io: File.open("app/assets/images/skull.png"), filename: "skull.png")
user_BOT = User.create({ slug: "bot", name: "Chirp Chat BOT", email: "chatbot@chirp.de", password: "asdasd", password_confirmation: "asdasd" })
user_BOT.image.attach(io: File.open("app/assets/images/bot.jpg"), filename: "bot.jpg")

user_alice.tweets.create({ content: "The greatest glory in living lies not in never falling, but in rising every time we fall. -Nelson Mandel" })
user_versicherungsmanager.tweets.create({ content: "Die Studie eines Rechtsprofessors schätzt, dass die Polizei nur 2 % der schweren Verbrechen aufklärt" })
user_versicherungsmanager.tweets.create({ content: "<img src='https://picsum.photos/200/300'>" })
user_blackhat.tweets.create({ content: "Remember that you will die, let that determine what you do and say and think." })
user_eve.tweets.create({ content: "The same men never steps in the same river twice." })
user_alice.tweets.create({ content: "Boa obs euch auch so gut geht? I daud it."})
user_blackhat.tweets.create({ content: "<img src='https://picsum.photos/200'>" })

conversation_alice_versicherungsmanager = Conversation.create({
  sender_id: 1,
  recipient_id: 2,
  read: true,
  created_at: "Thu, 25 Nov 2021 09:38:16 UTC +00:00",
  updated_at: "Thu, 25 Nov 2021 09:38:16 UTC +00:00"
})

conversation_alice_versicherungsmanager.messages.create({
  body: "Guten Tag Herr...
  Sie haben heute um 13:00 einen Termin mit der Firma Flaschenbrief. Die Unterlagen hinischtlich des neuen Vertrages mit der Firma habe ich Ihnen bereits via Email zugeschickt.
  ",
  user_id: 1,
  created_at: "Thu, 25 Nov 2021 09:40:16 UTC +00:00"
})

conversation_alice_versicherungsmanager.messages.create({
  body: "In Ordnung die Unterlagen habe ich erhalten. Danke dir.",
  user_id: 2,
  created_at: "Thu, 25 Nov 2021 09:45:16 UTC +00:00"
})
conversation_alice_versicherungsmanager.messages.create({
  body: "Kannst du mir einmal detailierte Informationen zum Thema Cross Site Scripting heraussuchen? Bezüglich Funktionsweise, Historie... Das IT-Department hat mir eine Frage diesbezüglich gestellt.",
  user_id: 2,
  created_at: "Thu, 25 Nov 2021 11:00:16 UTC +00:00"
})

conversation_alice_versicherungsmanager.messages.create({
  body: "Cross-Site-Scripting (XSS; deutsch Webseitenübergreifendes Skripting) bezeichnet das Ausnutzen einer Computersicherheitslücke in Webanwendungen, indem Informationen aus einem Kontext, in dem sie nicht vertrauenswürdig sind, in einen anderen Kontext eingefügt werden, in dem sie als vertrauenswürdig eingestuft werden. Aus diesem vertrauenswürdigen Kontext kann dann ein Angriff gestartet werden. https://de.wikipedia.org/wiki/Cross-Site-Scripting",
  user_id: 1,
  created_at: "Thu, 25 Nov 2021 11:33:16 UTC +00:00"
})

conversation_alice_versicherungsmanager.messages.create({
  body: "Du bist gefeuert, wenn Sie den Grund wissen möchten, können Sie den ja vielleicht auf Wikipedia finden.",
  user_id: 2,
  created_at: "Thu, 25 Nov 2021 12:21:16 UTC +00:00"
})

conversation_blackhat_versicherungsmanager = Conversation.create({
  sender_id: 3,
  recipient_id: 2,
  created_at: "Thu, 25 Nov 2021 11:38:16 UTC +00:00",
  updated_at: "Thu, 25 Nov 2021 11:38:16 UTC +00:00"
})

conversation_blackhat_versicherungsmanager.messages.create({
  body: "Sehr geehrte Blackhat Hacker,
  wir möchten Sie gerne für eine Operation einstellen, um Kunden für unsere Versicherung zu gewinnen. Die Zahlungsmodalitäten haben wir ja bereits abgesprochen.
  Mfg",
  user_id: 2,
  created_at: "Thu, 25 Nov 2021 10:05:16 UTC +00:00"
})

conversation_blackhat_versicherungsmanager.messages.create({
  body: "Vielen Dank für Ihr Vertrauen in unsere Leistung. Dann läuft ja alles nach Plan unseres Bosses Ereboss.",
  user_id: 3,
  created_at: "Thu, 25 Nov 2021 10:10:16 UTC +00:00"
})

conversation_bot_versicherungsmanager = Conversation.create({
  sender_id: 5,
  recipient_id: 2,
  created_at: "Thu, 25 Nov 2021 09:38:16 UTC +00:00",
  updated_at: "Thu, 25 Nov 2021 09:38:16 UTC +00:00"
})

conversation_bot_versicherungsmanager.messages.create({
  body: "Hi vielen Dank, dass Sie Chirp nutzen. Hier werden Ihnen wichtige Mitteilungen zugesendet.",
  user_id: 5,
  created_at: "Thu, 25 Nov 2021 10:05:16 UTC +00:00"
})
