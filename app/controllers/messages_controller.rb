# frozen_string_literal: true

class MessagesController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action do
    @conversation = Conversation.find(params[:conversation_id])
  end
  def index
    @messages = @conversation.messages
    if @messages.length > 10
      @over_ten = true
      @messages = @messages[-10..-1]
    end
    if params[:m]
      @over_ten = false
      @messages = @conversation.messages
    end
    if @messages.last
      if @messages.last.user_id != current_user.id
        @messages.last.read = true
      end
    end
    @message = @conversation.messages.new
  end

  def new
    @message = @conversation.messages.new
  end
  def create
    @sender = User.find(params[:message][:user_id])
    @message = @conversation.messages.new(message_params)
    csrf_token = params[:message][:csrf_token]
    if @message.save && (csrf_token == @sender.csrf_token)
      if @conversation.sender_id != current_user.id
        @conversation.update_column(:recipient_id, @conversation.sender_id)
        @conversation.update_column(:sender_id, current_user.id)
      end
      @conversation.update_column(:read, false)

      redirect_to conversation_messages_path(@conversation)
    end
  end

  private
    def message_params
      params.require(:message).permit(:body, :user_id)
    end
end
