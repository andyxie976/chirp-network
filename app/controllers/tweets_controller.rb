# frozen_string_literal: true

class TweetsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: [:destroy]

  def index
    @tweet = current_user.tweets.build if logged_in?
    @tweets = Tweet.includes(:user).with_attached_image.paginate(page: params[:page], per_page: 6)
    render "home/index"
  end

  def create
    number_of_chirps = 20
    if current_user.tweets.count < number_of_chirps
      @tweet = current_user.tweets.build(tweet_params)
      @tweet.image.attach(tweet_params[:image])
      if @tweet.save
        flash[:success] = "Chirp created!"
        redirect_to root_url
      else
        flash[:danger] = @tweet.errors.full_messages.to_sentence
        redirect_to root_url
      end
    else
      flash[:danger] = "No more than #{number_of_chirps} chirps are allowed"
    end
  end

  def destroy
    @tweet.destroy
    redirect_to root_url
  end

  private
    def tweet_params
      params.require(:tweet).permit(:content, :image)
    end

    def correct_user
      @tweet = current_user.tweets.find_by(id: params[:id])
      redirect_to root_url if @tweet.nil?
    end
end
