# frozen_string_literal: true

module ApplicationHelper
  def full_title(page_title)
    base_title = "Chirp"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def validation(text)
    substitute_script(substitute_window(text)).html_safe
  end

  def substitute_script(text)
    if text
      text.gsub("<script>", "").gsub("</script>", "")
    end
  end
  def substitute_window(text)
    if text
      text.gsub("window.location", "window location")
    end
  end
end
