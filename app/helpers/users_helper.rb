# frozen_string_literal: true

module UsersHelper
  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, size: 50, css_class: :gravatar)
    if user.image.attached?
      gravatar_url = user.image
    else
      gravatar_url = "default_profile.jpg"
    end
    image_tag(gravatar_url, size: 75, alt: user.name, class: css_class)
  end
end
