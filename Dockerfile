FROM ruby:3.0.3-alpine 
ENV PATH_PREFIX '/'
ENV http_proxy 'http://wwwproxy.uni-muenster.de:3128/'
ENV https_proxy 'http://wwwproxy.uni-muenster.de:3128/'
ENV RAILS_RELATIVE_URL_ROOT $PATH_PREFIX
ENV no_proxy 'localhost,127.0.0.1'
RUN apk add --no-cache --update \
    nodejs \
    yarn \
    build-base \
    sqlite-dev \
    python3 \
    py3-pip \
    py-cryptography \
    chromium-chromedriver \
    && rm -rf /var/cache/apk/*
RUN pip3 install selenium
RUN mkdir /app
COPY Gemfile Gemfile.lock package.json yarn.lock /app/
WORKDIR /app
RUN yarn install
RUN bundle install
COPY . ./
RUN chmod u+x ./xssBot.py
RUN rails db:create && \
    rails db:migrate && \
    rails db:reset
EXPOSE 3000
RUN chmod u+x ./entrypoint.sh
CMD [ "./entrypoint.sh" ]
