#!/bin/sh

set -e

if [ -f tmp/pids/server.pid ]; then
    rm tmp/pids/server.pid
fi

export RAILS_RELATIVE_URL_ROOT=$PATH_PREFIX
nohup python3 xssBot.py &
rails server -b 0.0.0.0
