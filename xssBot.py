#!/usr/bin/env python3

import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
#os.environ.get('PATH_PREFIX')
urlprefix = ""
baseurl = os.environ.get('BASE_URL') or 'http://localhost:3000'
if not 'localhost' in baseurl:
    baseurl = ':'.join(baseurl.split(':')[0:2])  #if not localhost, cut port

# chrome_options = Options()
# chrome_options.add_argument("--headless")
# chrome_options.add_argument("--no-sandbox")

# driver = webdriver.Chrome(options=chrome_options)

#time.sleep(20)

while True:
    try:
        url = f"{baseurl}{urlprefix}/login"
        driver.get(url)

        email = driver.find_element(By.ID, "session_email")
        password = driver.find_element(By.ID, "session_password")

        email.send_keys("versicherungsmanager@muensterschild.de")
        password.send_keys("asdasd")

        driver.find_element(By.NAME, "commit").click()

        url = f"{baseurl}{urlprefix}/chirps"
        driver.get(url)

        driver.close()

        print("done")
    except Exception as e:
        print(e)

    time.sleep(30)
