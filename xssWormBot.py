#!/usr/bin/env python3

import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
challenge_completed = False
link_checked = "https://www.google.de/"
flag = "Sehr geehrter Benutzer. Dein letzter Post hat 39.196.010 Personen erreicht."
urlprefix = os.environ.get('PATH_PREFIX')
baseurl = os.environ.get('BASE_URL') or 'http://localhost:3000'
if not 'localhost' in baseurl:
    baseurl = ':'.join(baseurl.split(':')[0:2])  #if not localhost, cut port

# chrome_options = Options()
# chrome_options.add_argument("--headless")
# chrome_options.add_argument("--no-sandbox")

#driver = webdriver.Chrome(options=chrome_options)

#time.sleep(20)

#Bob ist der Journalist
xss_bot = {"slug": "bot", "email": "chatbot@chirp.de", "password": "asdasd"}
users = [
    {"slug": "alice", "email": "alice@muensterschild.de", "password": "123123", "worm_completed": False},
    {"slug": "versicherungsmanager","email": "versicherungsmanager@muensterschild.de", "password": "asdasd", "worm_completed": False},
    {"slug": "blackhat","email": "blackhat@malware.de", "password": "qweqwe","worm_completed": False},
    {"slug": "eve","email": "Eve@evil.de", "password": "yxcyxc", "worm_completed": False},
]

def login(email, password):
    url = f"{baseurl}/login" #{urlprefix}
    driver.get(url)
    email_form = driver.find_element(By.ID, "session_email")
    password_form = driver.find_element(By.ID, "session_password")
    email_form.send_keys(email)
    password_form.send_keys(password)
    driver.find_element(By.NAME, "commit").click()
    url = f"{baseurl}/chirps" #{urlprefix}
    driver.get(url)

check_sum = 0
while not challenge_completed:
    try:
        for user in users:
            if(user["worm_completed"] == False):
                login(user["email"],user["password"])
                url = f"{baseurl}/users/"+user["slug"] #{urlprefix}
                driver.get(url)
                if (link_checked in driver.page_source):
                    user["worm_completed"] = True
                    check_sum += 1
        if check_sum == len(users):
            challenge_completed = True
    except Exception as e:
        print(e)

    if (challenge_completed):
        login(xss_bot["email"],xss_bot["password"])
        #time.sleep(1)
        url = f"{baseurl}/conversations."+xss_bot["slug"] #{urlprefix}
        driver.get(url)
        driver.find_element_by_xpath('//a[normalize-space()="Versicherungsmanager"]').click()
        #time.sleep(1)
        text_form = driver.find_element(By.ID, "message_body")
        text_form.send_keys(flag)
        driver.find_element(By.NAME, "commit").click()
        driver.close()

    #time.sleep(30)